const fs = require('fs');
const { spawn } = require('child_process');

function zipFolder (sourceFolder, destinationZip, finishedCallback, statusCallback){
	let files = fs.readdirSync(sourceFolder);
	
	if (fs.existsSync(destinationZip)){
		finishedCallback(false, "Destination file exists");
	}

	let fileCount = files.length;
	let filesDone = 0;

	let zip = spawn('zip', ['-9', '-r', '-j', destinationZip, sourceFolder]);

	zip.stdout.on('data', (data) => {
		//console.log(data.toString());

		if (data.toString().search("adding:") != -1 && data.toString().search(sourceFolder+" ") == -1){
			filesDone += 1;
			
			if (statusCallback != null){
				statusCallback(fileCount, filesDone);
			}
		}
	});

	zip.on('close', (code) => {
		if (code != 0){
			finishedCallback(false, "Zip software had non-zero exit code");
		} else {
			finishedCallback(destinationZip);
		}
	});
}

module.exports = zipFolder;
/*const Streamer = require('raspivid-stream');
const Websocket = require('ws');

let wss = new Websocket.Server({port: 3344});
console.log("Listening on 3344");
wss.on('connection', function connection(ws) {
    let stream = Streamer();

    stream.on('data', (data) => {
        ws.send(data, { binary: true }, (error) => { if (error) console.error(error); });
    })
});
*/

const AvcServer = require('./WSAvcServer.js');
const WebSocket = require('ws');
const spawn = require('child_process').spawn;

class Streamer {

    constructor(port, width, height) {
        this.port = port;
        this.width = width || 480;
        this.height = height || 320;
        this.isActive = false;

        this.wss = new WebSocket.Server({ port: this.port });
        console.log("Stream listening on port: " + this.port);

        this.avcServer = new AvcServer(this.wss, this.width, this.height);

        this.streamer = null;

        // OPTIONAL: start on connect
        this.avcServer.on('client_connected', (socket) => {
            if (!this.streamer && this.isActive) {
                this.startStreamer()
            } else {
                socket.close(1008);
            }

        });

        this.avcServer.on('client_disconnected', () => {
            console.log('client disconnected');
            if (this.avcServer.clients.size < 1) {
                if (!this.streamer) {
                    console.log('raspivid not running');
                    return;
                }
                console.log('stopping raspivid');
                this.streamer.kill('SIGTERM');
            }
        });
    }

    // Adding a method to the constructor
    start() {
        this.isActive = true;
    }

    stop(){
        this.isActive = false;
        this.wss.clients.forEach((socket) => {
            socket.close(1008);
        });
        this.streamer.kill('SIGTERM');
    }

    startStreamer() {
        console.log('starting raspivid');
        this.streamer = spawn('raspivid', [ '-pf', 'baseline', '-ih', '-t', '0', '-w', this.width, '-h', this.height, '-hf', '-fps', '15', '-g', '30', '-o', '-' ]);
        this.streamer.on('close', () => {
            this.streamer = null;
        });
        this.avcServer.setVideoStream(this.streamer.stdout);
    };
}

module.exports = Streamer;
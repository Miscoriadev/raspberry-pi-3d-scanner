const ping = require('ping');
const localIp = require('my-local-ip');
const net = require('net');

function discoverDevices (port, timeout, callback){
    let hosts = [];
    let ip = localIp();
    let ipElements = ip.split('.');

    for (let i = 0; i < 256; i++){
        hosts.push(ipElements[0] + '.' + ipElements[1] + '.' + ipElements[2] + '.' + i);
    }

    let index = hosts.indexOf(ip);
    hosts.splice(index, 1);

    hosts.forEach(async function(host){
        ping.sys.probe(host, async function(isAlive){
            let portAvailable = await isPortAvailable(port, {timeout: timeout, host: host});
            if (isAlive) console.log(`Found host ${host}, is port open: ${portAvailable}`);

            if (isAlive && portAvailable){
                callback(host);
            }
        });
    });
}

async function isPortAvailable(port, {timeout = 1000, host} = {}) {
	const promise = new Promise(((resolve, reject) => {
		const socket = new net.Socket();

		const onError = () => {
			socket.destroy();
			reject();
		};

		socket.setTimeout(timeout);
		socket.once('error', onError);
		socket.once('timeout', onError);

		socket.connect(port, host, () => {
			socket.end();
			resolve();
		});
	}));

	try {
		await promise;
		return true;
	} catch (_) {
		return false;
	}
};

module.exports = discoverDevices;
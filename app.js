const image_dir = "/var/www/html/images/";
const zip_dir = "/var/www/html/zips/";

const spawn = require('child_process').spawn;
const { execSync, exec } = require('child_process');
const WebSocket = require('ws');
const discoverDevices = require('./networking.js');
const IRSignalReceiver = require('./irsensor');
const StreamService = require('./streamer');
const fs = require("fs");
const zipFolder = require('./zipper');

const getVersionCommand = `git -C ${__dirname} describe --abbrev=0`;
const getCommitMessageCommand = `git -C ${__dirname} log -1 --pretty=%B`;
const getUpdateDateCommand = `git -C ${__dirname} log -1 --format=%cd`;

const getLatestImageCommand = `ls ${image_dir} | sort -n -t _ -k 2 | tail -1`;

let cameraOptions = [
    //'-r',               // Include RAW JPEG data
    //'-st',              // Force recomputation of statistics on stills capture pass
    '-n',               // No preview
    '-w', '3264',       // Width
    '-h', '2448',       // Height
    '-t', '50',        // Timeout
    '-q', '100',        // Quality 0-100
    '-ISO', '180',      // ISO
    '-ss',  '10000'     // Shutter time in micros, 10000 = 10ms
    // this one is added on snapshot '-o', 'test.jpg'    // Output file
];

let arguments = process.argv.slice(2);

/*discoverDevices((host) => {
    console.log(host);
});*/

let appPort = arguments[0];
let env = arguments[1];
let devMode = false;

if (env === "dev"){
    devMode = true;
}

console.log(execSync("apt install zip -y").toString());

console.log("Listening on port: " + appPort);
const wss = new WebSocket.Server({port: appPort || 6677});
let connections = [];
let cameraConnections = [];

let streamer = new StreamService(4888, '640', '480');
streamer.start();

let irReceiver = new IRSignalReceiver();

// If the zip dir does not exist create it.
if (!fs.existsSync(zip_dir)){
    fs.mkdirSync(zip_dir);
}

// If the image dir does not exist create it.
if (!fs.existsSync(image_dir)){
    fs.mkdirSync(image_dir);
}

wss.on('connection', function connection(ws) {

    ws.remoteAddress = ws._socket.remoteAddress.replace('::ffff:', '');
    console.log("New incoming connection: " + ws.remoteAddress);
    if (connections.length > 0){
        console.log("Closing existing connections...");
        connections.forEach((connection)=>{
            connection.close(1008);
        });

        irReceiver.stop();

        cameraConnections.forEach((cameraConnection)=>{
            cameraConnection.close(1008);
        });
        cameraConnections = [];

        let index = connections.indexOf(ws);
        if (index !== -1) connections.splice(index, 1);
    }

    connections.push(ws);

    ws.on('close', () => {
        let index = connections.indexOf(ws);
        if (index !== -1) connections.splice(index, 1);
    });

    ws.on('error', () => {
        console.log("Connection to %s failed.", host);
        let index = connections.indexOf(ws);
        if (index !== -1) connections.splice(index, 1);
    });

    ws.on('message', function incoming(data) {
        let message = JSON.parse(data);
        console.log(message);

        switch (message.type) {
            case 'snapshot':
                let imageName = 'image_' + new Date().toISOString() + '.jpg';
                makeSnapshot(image_dir + imageName, () => {
                    connections.forEach((connection) => {
                        connection.send(JSON.stringify({
                            "type":"snapshotResult",
                            "url":"http://" + connection._socket.localAddress.replace('::ffff:', '') + '/images/' + imageName,
                            "reference": message.reference || null
                        }))
                    });
                }, () => {
                    connections.forEach((connection) => {
                        connection.send(JSON.stringify({
                            "type":"snapshotFailed",
                            "reference": message.reference || null
                        }))
                    });
                });
                break;
            case 'masterSnapshot':
                cameraConnections.forEach((camConnection) => {
                    camConnection.send(JSON.stringify({
                        "type": "snapshot",
                        "reference": message.reference || null
                    }));
                });

                let imageName2 = 'image_' + new Date().toISOString() + '.jpg';
                makeSnapshot(image_dir + imageName2, () => {
                    connections.forEach((connection) => {
                        connection.send(JSON.stringify({
                            "type":"snapshotResult",
                            "url":"http://" + ws._socket.localAddress.replace('::ffff:', '') + '/images/' + imageName2,
                            "ip": ws._socket.localAddress.replace('::ffff:', ''),
                            "reference": message.reference || null
                        }))
                    });
                }, () => {
                    connections.forEach((connection) => {
                        connection.send(JSON.stringify({
                            "type":"snapshotFailed",
                            "ip": connection._socket.localAddress.replace('::ffff:', ''),
                            "reference": message.reference || null
                        }))
                    });
                });
                break;
            case 'shutdown':
                exec("sudo /sbin/shutdown now");
                break;
            case 'reboot':
                exec("sudo /sbin/reboot");
                break;
            case 'downloadZip':
                if (message.camera == ws._socket.localAddress.replace('::ffff:', '')){
                    makeZip(message);
                } else {
                    cameraConnections.forEach(camConnection => {
                        camConnection.send(JSON.stringify(message))
                    })
                }
                break;
            case 'clearImageDirectories':
                let rmdirresult = execSync(`rm -rf ${image_dir}*`);

                connections.forEach((connection) => {
                    connection.send(JSON.stringify({
                        "type":"imageDirectoryCleared",
                        "ip": connection._socket.localAddress.replace('::ffff:', ''),
                        "reference": message.reference || null
                    }))
                });
                break;            
            case 'masterShutdown':
                cameraConnections.forEach((camConnection) => {
                    camConnection.send(JSON.stringify({
                        "type": "shutdown",
                    }));
                });

                setTimeout(()=>{
                    exec("sudo /sbin/shutdown now");
                }, 10000);
                break;
            case 'masterReboot':
                cameraConnections.forEach((camConnection) => {
                    camConnection.send(JSON.stringify({
                        "type": "reboot",
                    }));
                });

                setTimeout(()=>{
                    exec("sudo /sbin/reboot");
                }, 10000);
                break;
            case 'masterClearImageDirectories':
                cameraConnections.forEach((camConnection) => {
                    camConnection.send(JSON.stringify({
                        "type": "clearImageDirectories",
                    }));
                });

                let rmdirresult2 = execSync(`rm -rf ${image_dir}*`);

                connections.forEach((connection) => {
                    ws.send(JSON.stringify({
                        "type":"imageDirectoryCleared",
                        "ip": connection._socket.localAddress.replace('::ffff:', ''),
                        "reference": message.reference || null
                    }))
                });
                break;        
            case 'becomeMaster':

                discoverDevices(4847, 800, (host) => {
                    console.log("Working on host: " + 'ws://' + host +':'+ appPort);
                    const nws = new WebSocket('ws://' + host +':'+ appPort);

                    nws.on('open', () => {
                        nws.remoteAddress = nws._socket.remoteAddress.replace('::ffff:', '');
                        cameraConnections.push(nws);
                        console.log("New connection to " + nws.remoteAddress);

                        // Send message to webpage taht a new camera has joined.
                        connections.forEach((connection) => {
                            connection.send(JSON.stringify({
                                "type":"join",
                                "ip": nws.remoteAddress
                            }))
                        });
                    });

                    nws.on('close', () => {
                        let index = cameraConnections.indexOf(nws);
                        if (index !== -1) cameraConnections.splice(index, 1);

                        let message = {
                            type: "leave",
                            ip: nws.remoteAddress,
                            reason: "Connection closed"
                        };

                        connections.forEach((connection) => {
                            connection.send(JSON.stringify(message));
                        });
                    });

                    nws.on('error', () => {
                        console.log("Connection to %s failed.", host);
                        let index = cameraConnections.indexOf(nws);
                        if (index !== -1) cameraConnections.splice(index, 1);
                    });

                    nws.on('message', (data) => {
                        let message = JSON.parse(data);
                        console.log(message);

                        switch (message.type){
                            case 'snapshotResult':
                            case 'snapshotFailed':
                            case 'join':
                            case 'version':
                            case 'imageDirectoryCleared':
                            case 'zipReady':
                            case 'zipFailed':
                            case 'zipProgress':
                                message.ip = nws.remoteAddress;
                                connections.forEach((connection) => {
                                    connection.send(JSON.stringify(message));
                                });
                                break;
                        }
                    })
                });

                if (!devMode){
                    // Start checking on IR signals and make picture on an incoming signal
                    irReceiver.start(()=>{
                        cameraConnections.forEach((camConnection) => {
                            camConnection.send(JSON.stringify({
                                "type": "snapshot",
                                "reference": message.reference || null
                            }));
                        });

                        let imageName2 = 'image_' + new Date().toISOString() + '.jpg';
                        makeSnapshot(image_dir + imageName2, () => {
                            connections.forEach((connection) => {
                                connection.send(JSON.stringify({
                                    "type":"snapshotResult",
                                    "url":"http://" + connection._socket.localAddress.replace('::ffff:', '') + '/images/' + imageName2,
                                    "ip": connection._socket.localAddress.replace('::ffff:', ''),
                                    "reference": message.reference || null
                                }))
                            });
                        }, () => {
                            connections.forEach((connection) => {
                                connection.send(JSON.stringify({
                                    "type":"snapshotFailed",
                                    "ip": connection._socket.localAddress.replace('::ffff:', ''),
                                    "reference": message.reference || null
                                }))
                            });
                        });
                    });
                    break;
                }

            default:
                break;
        }

    });

    connections.forEach((connection) => {
        connection.send(JSON.stringify({
            "type":"join",
            "ip": connection._socket.localAddress.replace('::ffff:', '')
        }))
    });

    sendVersionInfo();
});

function makeSnapshot(outputFile, callback, errorCallback){
    console.log("Making snapshot...");

    let camOpts = cameraOptions;
    camOpts.push('-o');
    camOpts.push(outputFile);

    var prc = spawn('raspistill', cameraOptions);
    prc.on('close', function (code) {
        if (code === 0){
            callback();
        } else {
            errorCallback();
        }
    });
}

function sendVersionInfo(){
    /**
     *  On a new connection send the most current version (based on GIT tags and messages)
     */
    let version = "unknown";
    try {
        version = execSync(getVersionCommand).toString().trim();
    } catch (e){
        console.log(e);
    }
    
    let commitMessage = "unknown";
    try {
        commitMessage = execSync(getCommitMessageCommand).toString().trim();
    } catch (e){
        console.log(e);
    }

    let updateDate = "unknown";
    try {
        updateDate = execSync(getUpdateDateCommand).toString().trim();
    } catch (e){
        console.log(e);
    }

    let latestImage = "unknown";
    try {
        latestImage = execSync(getLatestImageCommand).toString().trim();
    } catch (e){
        console.log(e);
    }

    connections.forEach((connection)=>{
        connection.send(JSON.stringify({
            "type":"version",
            "version": version,
            "message": commitMessage,
            "date": updateDate,
            "latestImage": (latestImage != "")?`http://${connection._socket.localAddress.replace('::ffff:', '')}/images/${latestImage}`:"undefined",
            "ip": connection._socket.localAddress.replace('::ffff:', '')
        }));
    });
}

function makeZip(message){
    let zipName = 'zip_' + new Date().toISOString() + '.zip';

    zipFolder(image_dir, zip_dir+zipName, 
    (result, error) => {
        console.log(`Zipping done: ${result}`);
        if (result){
            connections.forEach((connection) => {
                connection.send(JSON.stringify({
                    "type":"zipReady",
                    "source": `http://${connection._socket.localAddress.replace('::ffff:', '')}/zips/${zipName}`,
                    "ip": connection._socket.localAddress.replace('::ffff:', ''),
                    "reference": message.reference || null
                }))
            });
        } else {
            connections.forEach((connection) => {
                connection.send(JSON.stringify({
                    "type":"zipFailed",
                    "message": error,
                    "ip": connection._socket.localAddress.replace('::ffff:', ''),
                    "reference": message.reference || null
                }))
            });
        }
    },
    (filesTotal, filesDone) => {
        let percentage = (100/filesTotal)*filesDone;

        connections.forEach((connection) => {
            connection.send(JSON.stringify({
                "type":"zipProgress",
                "fileTotal": filesTotal,
                "filesDone": filesDone,
                "percentage": percentage.toFixed(2),
                "ip": connection._socket.localAddress.replace('::ffff:', ''),
                "reference": message.reference || null
            }))
        });
    });
}
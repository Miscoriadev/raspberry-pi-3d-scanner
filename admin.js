const WebSocket = require('ws');
const nws = new WebSocket('ws://192.168.178.22:1234');

nws.on('open', () => {
    nws.remoteAddress = nws._socket.remoteAddress;

    nws.send(JSON.stringify({
        "type": "becomeMaster"
    }));

    setInterval(() => {
        nws.send(JSON.stringify({
            "type": "previewSnapshot"
        }));
    },10000);
});

nws.on('close', () => {
});

nws.on('error', () => {
    console.log("Connection failed.");
});

nws.on('message', (data) => {
    //let message = JSON.parse(data);
    console.log(data);
});
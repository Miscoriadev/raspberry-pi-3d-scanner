const spawn = require('child_process').spawn;
const MIN_SIGNAL_TIMEOUT = 200;
const MAX_PULSE_BREAK = 50000;
const PULSE_COUNT_TO_SIGNAL = 20;

class IRSignal {

    constructor() {
        this.pulseCount = 0;
        this.lastSignal = new Date().getTime();
        this.isStarted = false;
    }

    // Adding a method to the constructor
    start(callback) {
        if (!this.isStarted) {
            this.isStarted = true;
            this.prc = spawn('mode2', ['-d', '/dev/lirc0']);

            this.prc.stdout.on('data', (data) => {
                let lines = data.toString().split("\n");
                if (lines.length > 0) {
                    for (let i = 0; i < lines.length; i++) {
                        let lineData = lines[i].split(' ');
                        if (lineData[1] < MAX_PULSE_BREAK) {
                            this.pulseCount++;
                        } else if (this.pulseCount > PULSE_COUNT_TO_SIGNAL) {
                            if ((new Date().getTime()) - this.lastSignal > MIN_SIGNAL_TIMEOUT) {
                                this.lastSignal = new Date().getTime();
                                callback(); // if it is too close to each other ignore the signal
                            }
                            this.pulseCount = 0;
                        } else {
                            this.pulseCount = 0;
                        }
                    }
                }
                //console.log(`Received chunk ${data}`);
            });

            this.prc.on('close', function (code) {
                console.log("IRreceiver stopped, restarting...");
                this.isStarted = false;
                start(callback);
            });
        }
    }

    stop(){
        if (typeof this.prc !== "undefined"){
            this.prc.kill();
        }
        this.isStarted = false;
    }
}

module.exports = IRSignal;